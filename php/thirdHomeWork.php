<?php

$users = [];
$users["3"] = ["name" => "Ya",
    "email" => "yayaya@gmail.com",
    "phone" => "+380973344222",
    "lang" => "en"];

$users["1"] = ["name" => "Slava",
    "email" => "sliva@gmail.com",
    "phone" => "+380927748222",
    "lang" => "en"];

$users["4"] = ["name" => "Sadori",
    "email" => "star.stat111@gmail.com",
    "phone" => "+380972355822",
    "lang" => "en"];

$users["2"] = ["name" => "Suzium",
    "email" => "suzka.uzka@gmail.com",
    "phone" => "+380445000522",
    "lang" => "en"];

$users["5"] = ["name" => "Viacheslav",
    "email" => "viachik@gmail.com",
    "phone" => "+380937854322",
    "lang" => "en"];

$languages = ["Українська", "Англійська", "Німецька", "Китайська", "Японська", "Французька"]
?>

<!DOCTYPE html>
<html lang="uk">
<head>
    <meta charset="utf-8">
    <title>Дз</title>
</head>
<body>
    <table border="2">
        <tr>
            <th>
                Простий вивід (
                <?php
                print_r(count($users));
                ?>
                користувачів)
            </th>
            <th>
                Відсортований список
            </th>
            <th>
                Користувачі із крайніми ID
            </th>
            <th>
                Користувачі, що біля крайніх ID
            </th>
            <th>
                Видалення користувача із мінімальним ID
            </th>
        </tr>
        <tr>
            <td>
                <?php
                    echo "<pre>";
                    print_r($users);
                    echo "</pre>";
                ?>
            </td>
            <td>
                <?php
                    echo "<pre>";
                    krsort($users);
                    print_r($users);
                    echo "</pre>";
                ?>
            </td>
            <td>
                <?php
                echo "<pre>";

                print_r(current($users));
                print_r(key($users));
                echo "<br><br>";
                print_r(end($users));
                print_r(key($users));

                echo "</pre>";
                ?>
            </td>
            <td>
                <?php
                echo "<pre>";

                reset($users);
                print_r(next($users));
                print_r(key($users));

                echo "<br><br>";

                end($users);
                print_r(prev($users));
                print_r(key($users));

                echo "</pre>";
                ?>
            </td>
            <td>
                <?php
                echo "<pre>";
                end($users);
                array_pop($users);
                print_r($users);
                echo "</pre>";
                ?>
            </td>
        </tr>
    </table>

    <label for="select"></label>
    <select id="select">
        <option value="">Оберіть мову</option>
        <?php
            foreach ($languages as $language) {
                echo "<option value=\"".$language."\">";
                echo $language;
                echo "</option>";
            }
        ?>
    </select>
</body>
</html>
