<?php



?>

<!DOCTYPE html>
<html lang="uk">
<head>
    <meta charset="utf-8">
    <title>New Дз</title>
</head>
<body>

<?php

// Перше завдання
$date = "31-12-2020";
$ans1 = "";
foreach (array_reverse(explode("-", $date)) as $num)
{
    $ans1 .= $num . ".";
}
$ans1 = rtrim($ans1, ".");

echo $ans1;
//

echo "<hr>";

// Друге завдання
$line2 = "london is the capital of great britain";
$ans2 = "";

foreach (explode(" ", $line2) as $word)
{
    $ans2 .= ucfirst($word) . " ";
}
$ans2 = rtrim($ans2);

echo $ans2;
//

echo "<hr>";

// Третє завдання
$password3 = "theRealPass";

if (mb_strlen($password3) >= 7 && mb_strlen($password3) <= 12)
{
    echo "Пароль пройшов верифікацію";
}
else
{
    echo "Пароль не пройшов верифікацію";
}
//

echo "<hr>";

// Четверте завдання
$line4 = "1a2b3c4b5d6e7f8g9h0";
$ans4 = "";

for ($i = 0; $i < mb_strlen($line4); ++$i)
{
    if (!is_numeric($line4[$i]))
    {
        $ans4 .= $line4[$i];
    }
}

echo $ans4;
//

echo "<hr>";

// Доп завдання

$text5 = "Главным фактором языка РНР является практичность. РНР должен предоставить программисту средства для быстрого и эффективного решения поставленных задач. Практический характер РНР обусловлен пятью важными характеристиками: традиционностью, простотой, эффективностью, безопасностью, гибкостью. Существует еще одна «характеристика», которая делает РНР особенно привлекательным: он распространяется бесплатно! Причем, с открытыми исходными кодами ( Open Source ). Язык РНР будет казаться знакомым программистам, работающим в разных областях. Многие конструкции языка позаимствованы из Си, Perl. Код РНР очень похож на тот, который встречается в типичных программах на С или Pascal. Это заметно снижает начальные усилия при изучении РНР. PHP — язык, сочетающий достоинства Perl и Си и специально нацеленный на работу в Интернете, язык с универсальным (правда, за некоторыми оговорками) и ясным синтаксисом. И хотя PHP является довольно молодым языком, он обрел такую популярность среди web-программистов, что на данный момент является чуть ли не самым популярным языком для создания web-приложений (скриптов).";
$lines5 = [""];
$lineId = 0;

foreach (explode(" ", $text5) as $word)
{
    if (mb_strlen($lines5[$lineId]) + mb_strlen($word) + 1 >= 80)
    {
        ++$lineId;
        $lines5[] = $word;
    }
    else
    {
        $lines5[$lineId] .= " ".$word;
    }
}
$lines5[0] = ltrim($lines5[0]);

for ($i = 0; $i < count($lines5); ++$i) {
    $k = 80 - mb_strlen($lines5[$i]);
    $n = count(explode(" ", $lines5[$i])) - 1;

    $newLine = "";
    $index = 0;
    $kdivn = floor($k / $n);
    $kmodn = $k % $n;

    foreach (explode(" ", $lines5[$i]) as $word)
    {
        $newLine .= $word . " "
            . str_repeat(" ", $kdivn + (($index < $kmodn) ? 1 : 0));
        ++$index;
    }

    $lines5[$i] = rtrim($newLine);
}

foreach ($lines5 as $line)
{
    echo "<pre>".$line."</pre>";
}

//
?>

</body>
