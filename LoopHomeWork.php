<?php

// Новий коментар
$new_users = [];
$new_users["3"] = [
    "name" => "Ya",
    "email" => "yayaya@gmail.com",
    "phone" => "+380973344222",
    "lang" => "en"
];

$new_users["1"] = [
    "name" => "Slava",
    "email" => "sliva@gmail.com",
    "phone" => "+380927748222",
    "lang" => "ua"
];

$new_users["4"] = [
    "name" => "Sadori",
    "email" => "star.stat111@gmail.com",
    "phone" => "+380972355822",
    "lang" => "ua"
];

$new_users["2"] = [
    "name" => "Suzium",
    "email" => "suzka.uzka@gmail.com",
    "phone" => "+380445000522",
    "lang" => "fr"
];

$new_users["5"] = [
    "name" => "Viacheslav",
    "email" => "viachik@gmail.com",
    "phone" => "+380937854322",
    "lang" => "fr"
];

$new_users["0"] = [
    "name" => "Viacheslav",
    "email" => "viachik@gmail.com",
    "phone" => "+380937854322",
    "lang" => "de"
];

$new_users["22"] = [
    "name" => "Suzium",
    "email" => "suzka.uzka@gmail.com",
    "phone" => "+380445000522",
    "lang" => "fr"
];

$new_users["222"] = [
    "name" => "Suzium",
    "email" => "suzka.uzka@gmail.com",
    "phone" => "+380445000522",
    "lang" => "fr"
];

$greetingsArray = [
    "en" => "Hello!",
    "ua" => "Привіт!",
    "fr" => "Bonjour!",
    "de" => "Hallo!",
];
?>

<!DOCTYPE html>
<html lang="uk">
<head>
    <meta charset="utf-8">
    <title>New Дз</title>
</head>
<body>
<?php

$dict = [];
foreach ($new_users as $user) {
    if (key_exists($user["name"], $dict)) {
        $dict[$user["name"]]++;
    } else {
        $dict[$user["name"]] = 1;
    }
}

print_r($dict);

echo "<hr>";

$usersByLang = [];
$finalUsersByLang = [];

foreach ($new_users as $user) {
    if (!key_exists($user["lang"], $usersByLang)){
        $usersByLang[$user["lang"]] = [];
        $usersByLang[$user["lang"]][] = $user;
    }
    else
    {
        # якщо елемент массива вже був доданий, то значить,
        # що вже один юзер в массиві є, тому під час цього else
        # вже буде 2 користувача у массиві
        $usersByLang[$user["lang"]][] = $user;
        $finalUsersByLang[$user["lang"]] = $usersByLang[$user["lang"]];
    }
}

echo "<pre>";
print_r($finalUsersByLang);
echo "</pre>";

echo "<hr>";

$reverseArr = [];
for ($i = count($new_users) - 1; $i >= 0; --$i)
{
    $reverseArr[array_keys($new_users)[$i]] = $new_users[array_keys($new_users)[$i]];
}

echo "<pre>";
print_r($reverseArr);
echo "</pre>";


?>

</body>
